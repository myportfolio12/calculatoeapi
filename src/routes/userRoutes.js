const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers");
const jwt = require("../config/jwt");
const { verify } = jwt;
router.post("/create", userControllers.createAccount);
router.post("/login", userControllers.login);
router.get("/user", verify, userControllers.userDetails);
router.post("/history", verify, userControllers.saveToHistory);
router.put("/history/clear", verify, userControllers.clearHistory);
router.put("/history/select-:id", verify, userControllers.selectHistory);

module.exports = router;
