const express = require("express");
const router = express.Router();
const calculatorControllers = require("../controllers/calculatorControllers");
const jwt = require("../config/jwt");
const { verify } = jwt;

router.post("/", verify, calculatorControllers.getString);
router.get("/history", verify, calculatorControllers.userHistory);
router.delete("/trash/:id", verify, calculatorControllers.deleteHistory);
router.delete("/allHistory", verify, calculatorControllers.deleteAllHistory);
module.exports = router;
