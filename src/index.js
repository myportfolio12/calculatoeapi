const express = require("express");
const cors = require("cors");
const db = require("./config/database");

const app = express();

app.use(cors());
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);
const userRoutes = require("./routes/userRoutes");
app.use("/", userRoutes);

const calculationRoutes = require("./routes/calculatorRoutes");
app.use("/calculator", calculationRoutes);

const port = 4000;
app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
