const User = require("../model/User");
const Calculator = require("../model/Calculator");
const bcrypt = require("bcrypt");
const jwt = require("../config/jwt");

const createAccount = async (req, res) => {
  try {
    const { username, email, password } = req.body;
    const isEmailExist = await User.findOne({ email });
    const hashedPass = bcrypt.hashSync(password, 12);

    if (username == "" || email == "" || password == "") {
      return res
        .status(400)
        .json({ status: false, message: "fields must not be empty" });
    }
    if (isEmailExist) {
      return res.status(409).json({
        status: false,
        message: "Email Exist",
      });
    }
    const user = new User({
      username,
      email,
      password: hashedPass,
    });
    await user.save();
    return res.status(201).json({
      status: true,
      message: "Successfully create Account",
      user,
      calculator,
    });
  } catch (error) {
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retrieve the user data." });
  }
};

const login = async (req, res) => {
  try {
    const { email, password } = req.body;
    const user = await User.findOne({ email });
    if (!user) {
      return res
        .status(401)
        .json({ status: false, message: "Invalid email or password" });
    }
    const isPasswordCorrect = bcrypt.compareSync(password, user.password);

    if (!isPasswordCorrect) {
      return res
        .status(401)
        .json({ status: false, message: "Invalid email or password" });
    }
    const token = await jwt.createAcessToken(user);
    return res.status(200).json({ status: true, token, user });
  } catch (error) {
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retrieve the user data." });
  }
};
const userDetails = async (req, res) => {
  try {
    const token = await jwt.decode(req.headers.authorization);
    if (!token) {
      return res
        .status(403)
        .json({ status: false, message: "Forbidden Action" });
    }
    const user = await User.findOne(token);
    if (!user) {
      return res.status(404).json({ status: false, message: "User not found" });
    }
    return res.status(200).json({ user, token: token.id });
  } catch (error) {
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retrieve the user data." });
  }
};
const saveToHistory = async (req, res) => {
  try {
    const token = await jwt.decode(req.headers.authorization);
    console.log(token.userId);
    const user = await User.findOne({ _id: token.userId });
    console.log(user);
    const { calcProblem, calcSolution, calcAnswer } = req.body;
    if (!token || !user) {
      return res.send(403).json({ status: false, message: "Access denied" });
    }
    user.calculationHistory.push({ calcProblem, calcSolution, calcAnswer });
    user.save();
    return res.status(200).json({ status: true, message: "Save history" });
  } catch (error) {
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retrieve the user data." });
  }
};
const clearHistory = async (req, res) => {
  try {
    const token = await jwt.decode(req.headers.authorization);
    const user = await User.findOne({ _id: token.userId });
    if (!token || !user) {
      return res.send(403).json({ status: false, message: "Access denied" });
    }
    user.calculationHistory = [];
    user.save();
    return res.status(200).json({ status: true, message: "Clear History" });
  } catch (error) {
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retrieve the user data." });
  }
};
const selectHistory = async (req, res) => {
  try {
    const historyId = req.params.id;
    const userHistory = await User.findOne({
      "calculationHistory._id": historyId,
    });
    console.log(userHistory);
  } catch (error) {
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retrieve the user data." });
  }
};
module.exports = {
  createAccount,
  login,
  userDetails,
  saveToHistory,
  clearHistory,
  selectHistory,
};
