const mongoose = require("mongoose");
const formattedDate = `${String(new Date().getMonth() + 1).padStart(
  2,
  "0"
)}/${String(new Date().getDate()).padStart(
  2,
  "0"
)}/${new Date().getFullYear()} - ${String(new Date().getHours() % 12).padStart(
  2,
  "0"
)}:${String(new Date().getMinutes()).padStart(2, "0")} ${
  new Date().getHours() >= 12 ? "pm" : "am"
}`;
const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  calculationHistory: [
    {
      calcProblem: {
        type: String,
        required: true,
      },
      calcAnswer: {
        type: String,
        required: true,
      },
      isSelect: {
        type: Boolean,
        default: false,
      },
      timestamp: {
        type: String,
        default: formattedDate,
        required: true,
      },
    },
  ],
});

const User = mongoose.model("User", userSchema);

module.exports = User;
