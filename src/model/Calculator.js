const mongoose = require("mongoose");
const formattedDate = `${String(new Date().getMonth() + 1).padStart(
  2,
  "0"
)}/${String(new Date().getDate()).padStart(
  2,
  "0"
)}/${new Date().getFullYear()} - ${String(new Date().getHours() % 12).padStart(
  2,
  "0"
)}:${String(new Date().getMinutes()).padStart(2, "0")} ${
  new Date().getHours() >= 12 ? "pm" : "am"
}`;

const calculatorSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
  calculationHistory: [
    {
      calcProblem: {
        type: String,
        required: true,
      },
      calcSolution: {
        type: String,
        required: true,
      },
      calcAnswer: {
        type: String,
        required: true,
      },
      timestamp: {
        type: String,
        default: formattedDate,
        required: true,
      },
    },
  ],
});

const Calculator = mongoose.model("Calculator", calculatorSchema);
module.exports = Calculator;
