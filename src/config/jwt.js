const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");

dotenv.config();

const createAcessToken = async (details) => {
  const data = {
    calcId: details.calcId,
    userId: details.id,
    email: details.email,
  };

  return jwt.sign(data, process.env.JWT_SECRET, {});
};

const verify = async (req, res, next) => {
  let token = req.headers.authorization;
  if (!token) {
    return res.status(404).json({ auth: "failed" });
  } else {
    token = token.slice(7);
    return jwt.verify(token, process.env.JWT_SECRET, (err, data) => {
      if (err) {
        return res.send({ auth: "failed" });
      } else {
        req.user = data;
        next();
      }
    });
  }
};

const decode = async (token) => {
  if (!token) {
    return null;
  } else {
    token = token.slice(7);

    return jwt.verify(token, process.env.JWT_SECRET, (err, data) => {
      if (err) {
        return null;
      } else {
        return data;
      }
    });
  }
};

module.exports = {
  createAcessToken,
  verify,
  decode,
};
