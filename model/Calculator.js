const mongoose = require("mongoose");
const currentDate = new Date();
const month = String(currentDate.getMonth() + 1).padStart(2, "0");
const day = String(currentDate.getDate()).padStart(2, "0");
const year = currentDate.getFullYear();
const hours = String(currentDate.getHours()).padStart(2, "0");
const minutes = String(currentDate.getMinutes()).padStart(2, "0");
const ampm = hours >= 12 ? "pm" : "am";
const formattedDate = `${month}/${day}/${year} - ${
  hours % 12
}:${minutes} ${ampm}`;

const calculatorSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: true,
  },
  timestamp: {
    type: String,
    default: formattedDate,
    required: true,
  },
  calculationString: {
    type: String,
    required: true,
  },
});

const Calculator = mongoose.model("Calculator", calculatorSchema);
module.exports = Calculator;
