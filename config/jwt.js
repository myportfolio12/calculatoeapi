const jwt = require("jsonwebtoken");
const secret = "calculator";

const createAcessToken = async (details) => {
  const data = {
    userId: details.id,
    email: details.email,
  };
  return jwt.sign(data, secret, {});
};

const verify = async (req, res, next) => {
  let token = req.headers.authorization;
  if (!token) {
    return res.status(404).json({ auth: "failed" });
  } else {
    token = token.slice(7);
    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        return res.send({ auth: "failed" });
      } else {
        req.user = data;
        next();
      }
    });
  }
};

const decode = async (token) => {
  if (!token) {
    return null;
  } else {
    token = token.slice(7);

    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        return null;
      } else {
        return jwt.decode(token, { complete: true }).payload;
      }
    });
  }
};

module.exports = {
  createAcessToken,
  verify,
  decode,
};
