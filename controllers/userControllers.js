const User = require("../model/User");
const bcrypt = require("bcrypt");
const jwt = require("../config/jwt");

const createAccount = async (req, res) => {
  try {
    const { firstName, lastName, email, password } = req.body;
    const isEmailExist = await User.findOne({ email });
    const hashedPass = bcrypt.hashSync(password, 12);

    if (firstName == "" || lastName == "" || email == "" || password == "") {
      return res
        .status(400)
        .json({ status: false, message: "fields must not be empty" });
    }
    if (isEmailExist) {
      return res.status(409).json({
        status: false,
        message: "Email Exist",
      });
    }
    const user = new User({
      firstName,
      lastName,
      email,
      password: hashedPass,
    });
    await user.save();
    return res.status(201).json({
      status: true,
      message: "Successfully create Account",
      user,
    });
  } catch (error) {
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retrieve the user data." });
  }
};

const login = async (req, res) => {
  try {
    const { email, password } = req.body;
    const user = await User.findOne({ email });
    if (!user) {
      return res
        .status(401)
        .json({ status: false, message: "Invalid email or password" });
    }
    const isPasswordCorrect = bcrypt.compare(password, user.password);
    if (!isPasswordCorrect) {
      return res
        .status(401)
        .json({ status: false, message: "Invalid email or password" });
    }
    const token = await jwt.createAcessToken(user);
    return res.status(200).json({ status: true, token, user });
  } catch (error) {
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retrieve the user data." });
  }
};
const userDetails = async (req, res) => {
  try {
    const token = jwt.decode(req.headers.authorization);
    const user = await User.findOne(token.userId);
    return res.status(200).json({ user });
  } catch (error) {
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retrieve the user data." });
  }
};
module.exports = {
  createAccount,
  login,
  userDetails,
};
