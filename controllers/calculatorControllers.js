const Calculator = require("../model/Calculator");
const User = require("../model/User");
const jwt = require("../config/jwt");

const getString = async (req, res) => {
  try {
    const token = jwt.decode(req.headers.authorization);
    const { calculationString } = req.body;
    const user = await User.findOne(token.userId);
    if (!user) {
      return res.status(404).json({ status: false, message: "User not found" });
    }

    const calculator = new Calculator({
      userId: user.id,
      calculationString,
    });
    await calculator.save();
    return res.status(200).json({ status: true, calculator });
  } catch (error) {
    console.log(error);
    res
      .status(500)
      .json({ status: false, message: "Failed to retrieve the user data." });
  }
};
const userHistory = async (req, res) => {
  const token = jwt.decode(req.headers.authorization);
  const userHistory = await Calculator.find(token.userId);
  if (!userHistory) {
    return res.status(404).json({ status: false, messag: "History Empty" });
  }
  return res.status(200).json({ status: true, userHistory });
};

const deleteHistory = async (req, res) => {
  const { id } = req.params;
  const userHistory = await Calculator.findOne({
    _id: id,
  });
  if (!userHistory) {
    return res.status(404).json({ status: false, messag: "Item not found" });
  }
  await Calculator.deleteOne({ _id: id });
  return res
    .status(200)
    .json({ status: true, messag: "Succesfully deleted history" });
};
const deleteAllHistory = async (req, res) => {
  const token = jwt.decode(req.headers.authorization);
  const userHistory = await Calculator.find(token.userId);
  if (userHistory.length === 0) {
    return res.status(404).json({ status: false, messag: "History Empty" });
  }
  await Calculator.deleteMany(token.userId);
  return res
    .status(200)
    .json({ status: true, messag: "Succesfully deleted history" });
};
module.exports = {
  getString,
  userHistory,
  deleteHistory,
  deleteAllHistory,
};
